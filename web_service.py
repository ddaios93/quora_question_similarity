# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 12:19:03 2019

@author: daiosd
"""

from datetime import timedelta
from flask import Flask, Response, Blueprint, request, make_response, current_app
from flask_restplus import Resource, Api, fields
#from flask_cors import CORS

import pandas as pd
import numpy as np
import os
from keras.models import load_model

from load_glove_embeddings import load_glove_embeddings
from utils import remove_long_questions, process_sentences, flag_non_embedded_sequences, custom_tokenize
from keras.preprocessing.sequence import pad_sequences
####from keras.np_utils import probas_to_classes

app = Flask(__name__)
api = Api(app, title='Question Similarity API')

blueprint = Blueprint('api', __name__, url_prefix='/api')
api.init_app(blueprint)
app.register_blueprint(blueprint)

api.namespaces.clear()
ns = api.namespace('Test ML model',
                   description='Operations related to question checking')

port = int(os.getenv("PORT", 9099))

question_fields = api.model('Question_Object', {
    "question1": fields.String(required=True, description='...', example='How do I improve my pronunciation of English?'),
    "question2": fields.String(required=True, description='...', example='What is the best path I should take to improve my English?'),
})
question_fields_temp = api.model('Question_Object_temp', {
    "question1": fields.String(required=True, description='...', example='What is the best school for Jiu Jitsu in California?'),
    "question2": fields.String(required=True, description='...', example='Can you study Gracie jiu jitsu after 50?'),
})
question_fields_3 = api.model('Question_Object_temp_3', {
    "question1": fields.String(required=True, description='...', example='Is this an apple?'),
    "question2": fields.String(required=True, description='...', example='Is this an pdapwd?'),
})

test_fields = api.model('Question_checking_list', {

    "first_question": fields.Nested(question_fields, required=True, description='First question'),
    "second_question": fields.Nested(question_fields_temp, required=True, description='Second question'),
    "third_question": fields.Nested(question_fields_3, required=True, description='Third question')
})

import json
@ns.route('/similarity_check/', methods=["post"])
class RunSimulationsScenario(Resource):
#    @ns.doc(description=" ")

    @api.expect(test_fields)
    def post(self):
        '''
        Prediction of question similarity using the off-line trained model
        '''
        app.logger.info((request.json))
        user_prefs = pd.DataFrame(request.json)
        app.logger.info((user_prefs))
        
        user_prefs = user_prefs.T
        data = remove_long_questions( user_prefs.copy(), max_q_len=30 )
        data = process_sentences(data)
        data.to_csv('temp.csv')
        
        word2index, embedding_matrix = load_glove_embeddings('data_and_models/glove.6B.50d.txt', embedding_dim=50)
        data['q1_flags'] = flag_non_embedded_sequences(data, 'question1', word2index).values()
        data['q2_flags'] = flag_non_embedded_sequences(data, 'question2', word2index).values()
        data['drop'] = data['q1_flags'] +  data['q2_flags']
        data = data[data['drop']==0]
        
        data= data[['question1','question2']] 
        back_up_index = data.index
        #data.to_csv('temp_2.csv')

        question_1_seq = custom_tokenize(data, 'question1', word2index)
        question_2_seq = custom_tokenize(data, 'question2', word2index)
        
        maxlen = 30
        # PAD WITH 400,000 because - (blank): '' is represented with the embending at this potition
        question_1_seq = pad_sequences(question_1_seq , maxlen=maxlen, padding='post',value=400000)
        question_2_seq = pad_sequences(question_2_seq , maxlen=maxlen, padding='post',value=400000)
        
        # load model
        model = load_model('data_and_models/weights.best.hdf5')
        pred = model.predict([question_1_seq, question_2_seq])
        pred = np.where( pred>0.5, 1, 0)
        pred = pd.DataFrame(pred, index=back_up_index, columns = ['pred_result'])
        user_prefs = user_prefs.merge(pred, how ='left', left_index=True, right_index=True)      
        user_prefs['pred_result'] = user_prefs['pred_result'].fillna('error misspeling')
        #user_prefs.to_csv('temp_3.csv')

        resp = Response(
            user_prefs.to_json(orient='index'),
            status=200,
            mimetype="application/json")
        return(resp)
        

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port = port)
