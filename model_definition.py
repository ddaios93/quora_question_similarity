from keras.models import Model
from keras.layers import Input
from keras.models import Sequential
from keras.layers import Dropout, Embedding, Flatten, Dense, LSTM, concatenate , Bidirectional, BatchNormalization, Activation
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2

def sequence_model(embedding_matrix, maxlen):

	lstm = LSTM(64, activation='tanh')

	embedding_layer = Embedding(input_dim=embedding_matrix.shape[0],
								output_dim=embedding_matrix.shape[1], 
								input_length=maxlen,
								weights=[embedding_matrix], 
								trainable=False, 
								name='embedding_layer')

	i_left = Input(shape=(maxlen,), dtype='int32', name='left_input')
	left_input = embedding_layer(i_left)
	left_output = lstm(left_input)

	i_right = Input(shape=(maxlen,), dtype='int32', name='right_input')
	right_input = embedding_layer(i_right)
	right_output = lstm(right_input)

	merged = concatenate([left_output, right_output])
	merged = Dropout(rate=0.2)(merged)       #############
	predictions = Dense(100)(merged)
	predictions = BatchNormalization()(predictions)     ###########
	predictions = Activation('relu')(predictions)

	predictions = Dense(1, activation='sigmoid')(predictions)

	model = Model([i_left, i_right], predictions)
	return model
