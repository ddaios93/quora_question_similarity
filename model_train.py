import pandas as pd
import numpy as np
from utils import custom_tokenize
from load_glove_embeddings import load_glove_embeddings
from keras.preprocessing.sequence import pad_sequences
from data_transform import data_processor
from model_definition import sequence_model
from keras.callbacks import ModelCheckpoint
np.random.seed(42)

# step 1
word2index, embedding_matrix = load_glove_embeddings('data_and_models/glove.6B.50d.txt', embedding_dim=50)
data = data_processor( word2index )

# step 2
question_1_seq = custom_tokenize(data, 'question1', word2index)
question_2_seq = custom_tokenize(data, 'question2', word2index)

maxlen = 30
# PAD WITH 400,000 because - (blank): '' is represented with the embending at this potition
question_1_seq = pad_sequences(question_1_seq , maxlen=maxlen, padding='post',value=400000)
question_2_seq = pad_sequences(question_2_seq , maxlen=maxlen, padding='post',value=400000)

#DEFINE MODEL
model = sequence_model(embedding_matrix, maxlen)

# checkpoint
filepath="data_and_models/weights.best.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
callbacks_list = [checkpoint]

model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])  # Compile the model
print(model.summary())  # Summarize the model
model.fit([question_1_seq, question_2_seq], data['is_duplicate'].values, validation_split=0.2, batch_size=128, epochs=15, callbacks=callbacks_list,verbose=2)  # Fit the model
# I used the automated option to split between validation and train set : validation_split=0.2
# I read the documentation and this is equivalent to spliting as: validation = data[ int((1 - 0.2)*data.shape[0]) ]
# Or in other words take the last 20% of the samples. I checked the ratio of positive to negative classes and its almost
# similar to the total ratio. ( 61% to 39%)

## loss, accuracy = model.evaluate([question_1_seq, question_2_seq], data['is_duplicate'].values, verbose=0)  # Evaluate the model
## print('Accuracy: %0.3f' % accuracy) # no need to do it on the whole dataset
