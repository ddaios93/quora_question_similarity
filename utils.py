import pandas as pd
import numpy as np
import re
from dict_load import get_dict

def remove_long_questions( data, max_q_len=30 ):
	# calculate sentence len
	data['len_1'] = data['question1'].str.split().apply(len)
	data['len_2'] = data['question2'].str.split().apply(len)
	#filter out
	data['valid'] = np.where( ((data['len_1']<=max_q_len) & (data['len_2']<=max_q_len)) &
                           ((data['len_1']>2) & (data['len_2']>2)) , 1,0  )
	data = data[data['valid']==1]
	return data

def process_sentences(data):
	# lower case
	data['question1'] = data['question1'].astype(str).apply(lambda x: x.lower())
	data['question2'] = data['question2'].astype(str).apply(lambda x: x.lower())
	# words normalisation, such as 'what's' to 'what is' + convert currencies to 'usd'
	data.replace( get_dict() ,regex=True,inplace=True)
	# remove punctuation characters
	data['question1'] = data['question1'].apply((lambda x: re.sub('[^a-zA-z0-9\s]',' ',x)))
	data['question2'] = data['question2'].apply((lambda x: re.sub('[^a-zA-z0-9\s]',' ',x)))
	# replace number strings with the word 'number'
	data['question1'] = data['question1'].apply((lambda x: re.sub('[0-9]*[0-9]', ' number ',x))) 
	data['question2'] = data['question2'].apply((lambda x: re.sub('[0-9]*[0-9]', ' number ',x)))
	# remove blanks
	data['question1'] = data['question1'].str.split()
	data['question2'] = data['question2'].str.split()
	return data
	
def flag_non_embedded_sequences(data, arg_name, word2index):
	
	flags={}
	for i in range(data.shape[0]):
		flags[i] = 0
		for q in data[ arg_name ].iloc[i]:
			if q not in word2index.keys():
				flags[i] = 1
	return flags
	
def custom_tokenize(data, arg_name, word2index):
	
	out_matrix = []
	for i in range(data.shape[0]):
		indice = list()
		for q in data[ arg_name ].iloc[i]:
			indice.append( word2index[q] )
		out_matrix.append( indice )
	return out_matrix
