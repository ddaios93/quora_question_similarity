# quora_question_similarity
This project is an implement ion for a simple question similarity matching in quora dataset. I have attached the dataset on the git. The only thing you are going to need is to download the Glove embeddings.

## Glove embeddings
Download the  Wikipedia 2014 + Gigaword 5 (6B tokens, 400K vocab, uncased, 50d, 100d, 200d, & 300d vectors, 822 MB download): glove.6B.zip, from https://nlp.stanford.edu/projects/glove/. 
Extract the .txt files and keep the one with name: 'glove.6B.50d.txt'. Move that  file in the folder "data_and_models"

## Train the model off-line
The model is a combination of 2 parallel LSTM which share weights. There are also some dense layers. 

To train the model simply run: python model_train.py

This will do all the preprocessing and during training, it will save the model with the best validation accuraccy inside the folder "data_and_models"

To test the web service API there is no need to wait for the full 15 epochs to finish. You can train the model for couple of epochs, let the callback save the best model at this point and then terminate the process with CTRL+C

The accuracy of the model after 15 epochs should be about 78%+ in the validation set. (Ratio of classes is 69% to 31%)

## Test online web-service API
To test the API just run the command: python web_service.py

The API uses the Flask and Flask-restplus libraries. Then open your browser and type: localhost:9099/

I have already create and template of questions. So open the 'Post' API and execute the request using the predefined Json format. You can change the template questions to whatever you like in order to test the functionality of the service.

## Considerable Credits to:
https://github.com/howardyclo/Kaggle-Quora-Question-Pairs
https://jovianlin.io/embeddings-in-keras/
https://www.kaggle.com/life2short/data-processing-replace-abbreviation-of-word 