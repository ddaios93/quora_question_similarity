import pandas as pd
import numpy as np
from load_glove_embeddings import load_glove_embeddings
from utils import remove_long_questions, process_sentences, flag_non_embedded_sequences

def data_processor( word2index ):
	data = pd.read_csv('data_and_models/quora_duplicate_questions.tsv', sep='\t').set_index(['id', 'qid1', 'qid2']).fillna('0')

	data = remove_long_questions( data, max_q_len=30 )
	data = process_sentences(data)

	#word2index, embedding_matrix = load_glove_embeddings('glove.6B.50d.txt', embedding_dim=50)

	data['q1_flags'] = flag_non_embedded_sequences(data, 'question1', word2index).values()
	data['q2_flags'] = flag_non_embedded_sequences(data, 'question2', word2index).values()
	data['drop'] = data['q1_flags'] +  data['q2_flags']
	data = data[data['drop']==0]

	new_data= data[['question1','question2','is_duplicate']]

	return new_data 
	#new_data.to_csv('transformed_data.csv')
